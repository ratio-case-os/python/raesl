// TEST
digraph G {
	graph [rankdir=TD]
	subgraph cluster_world {
		label=world
		subgraph "cluster_world.drive-mechanism" {
			label="world.drive-mechanism"
			subgraph "cluster_world.drive-mechanism.power-source" {
				label="world.drive-mechanism.power-source"
				"world.drive-mechanism.power-source.convert-potential" [shape=ellipse]
			}
			"world.drive-mechanism.bs-provide-power" [shape=hexagon]
			subgraph "cluster_world.drive-mechanism.motor" {
				label="world.drive-mechanism.motor"
				"world.drive-mechanism.motor.convert-power" [shape=ellipse]
			}
		}
		"world.provide-torque" [shape=hexagon]
		subgraph "cluster_world.pump" {
			label="world.pump"
			"world.pump.convert-torque" [shape=ellipse]
		}
	}
	"world.provide-torque" -> "world.pump.convert-torque" [style=solid]
	"world.drive-mechanism.bs-provide-power" -> "world.drive-mechanism.motor.convert-power" [style=solid]
	"world.drive-mechanism.power-source.convert-potential" -> "world.drive-mechanism.bs-provide-power" [style=solid]
	"world.drive-mechanism.motor.convert-power" -> "world.provide-torque" [style=solid]
}
