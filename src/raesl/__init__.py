"""Ratio ESL

Ratio ESL support in Python.
"""

__version__ = "0.14.4"


import logging

logger = logging.getLogger("raesl")
logger.setLevel(logging.INFO)
